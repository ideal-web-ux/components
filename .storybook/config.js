import { configure } from '@storybook/react';
import { checkA11y } from '@storybook/addon-a11y';

// addDecorator(checkA11y);  // This if uncommented is breaking the storybook
const req = require.context('../src/components', true, /.stories.tsx$/);
function loadStories() {
  req.keys().forEach(req);
}
configure(loadStories, module);
