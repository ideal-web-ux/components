import React, { Component } from 'react';
import { Coordinate } from './pie.utils';

interface PieChartProps{
    percentages: any
}

// import styles from './pie.scss';
var styles = require('./pie.module.scss');

export default class PieChart extends Component<PieChartProps, {}> {
    previousPercentageAccumulator = 0;

    render() {
        return (
            <div>
                <svg viewBox="0 0 100 100">

                    <g className={styles['pie-slices']}>
                        { this.renderPies() }
                    </g>
                    <g className={styles['pie-animations']}>
                        <circle className={`${styles['pie-part']} ${styles['circle-animate']} ${styles['pie-part--full']}`}></circle>
                    </g>
                    <g>
                        { this.renderTextForPies() }
                    </g>
                </svg>
            </div>
        );
    }

    renderPies = ():Element => 
        this.props.percentages.map((m:string, i: number, arr: Array<string>) => 
            <circle className={styles['pie-part']} ref={"pie-part"} style={this.piePartSpecificStyles(m, i, arr)}></circle>);

    renderTextForPies = ():Element => this.props.percentages.map((m:string, i: number) => <text { ...this.placeTextForPie(m, i) }>{m}%</text>);

    placeTextForPie = (pie: string, pieIndex: number):Coordinate => {
        // @TODO: Understand and implement the formula needed to put the percentage text
        // in exact coordinates based on dynamic pie counts and different percentage values

        // @TEMP
        var dummyObjectForThreeFixedPies:Array<Coordinate> = [
            {
                x:"12",
                y:"50"
            },
            {
                x:"50",
                y:"75"
            },
            {
                x:"62", 
                y:"37"
            }
        ]

        return dummyObjectForThreeFixedPies[pieIndex];
    }

    piePartSpecificStyles(percentage:any, index: number, wholeArray:Array<string>){
        var length = wholeArray.length;
        var pi = 22/7;
        // var strokeWidth = styles['pie-part']['stroke-width'];
        var strokeWidth = 50;
        // var element =  this.refs["pie-part"];
        // var strokeWidth = window.getComputedStyle(ReactDOM.findDOMNode(element)).getPropertyValue("stroke-width");
        var iterateeRange = (index / length) * 100;
        var degreesToRotate =  (360 * this.previousPercentageAccumulator / 100) - 90;
        var circumference = strokeWidth * pi;
        var percentageSector = strokeWidth * pi * percentage / 100;
        var colorValue = 60 + (300 * iterateeRange);
        var styleObj = {
            "stroke": `hsla(${colorValue}, 100%, 34%, 1)`,
            // mention only once for reverse pie drawing
            "stroke-dasharray": `${percentageSector} ${circumference}`,
            "transform": `rotate(${degreesToRotate}deg)`,
        }
        
        this.previousPercentageAccumulator = +percentage + this.previousPercentageAccumulator;

        return styleObj;
    }
}
