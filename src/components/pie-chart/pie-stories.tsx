import { checkA11y } from '@storybook/addon-a11y';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import PieChart from "./pie";
storiesOf("PieChart", module)
    .addDecorator(checkA11y)
    .add("with text", () => {
        var percentages = ['10', '35', '55'];

        return <PieChart percentages={percentages}></PieChart>
    });