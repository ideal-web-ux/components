import * as React from 'react';
// import * as styles from './button.styles.scss';

var componentStyles = require('./button.module.scss');

export interface IButtonProps {
  children?: React.ReactNode,
  onClick?: (e:any) => void,
  styles?: {}
}
const styles = {
  border: '1px solid #eee',
  borderRadius: 3,
  cursor: 'pointer',
  fontSize: 15,
  padding: '3px 10px',
  margin: 10,
};
const Button: React.SFC<IButtonProps> = (props) => (
  <button onClick={props.onClick} style={ styles } className={ componentStyles.button } type="button">
    {props.children}
  </button>
);
Button.defaultProps = {
  children: null,
  onClick: () => {}
};
export default Button;